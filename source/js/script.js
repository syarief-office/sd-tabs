/* Author:http://www.rainatspace.com

*/
function initializeScript(){
    "use strict";

	jQuery(".tabs_nav li").click(function() {
		jQuery(".tabs_nav li").removeClass('active');
		jQuery(this).addClass("active");
		jQuery(".tabs_content").hide();
		var selected_tab = jQuery(this).find("a").attr("href");
		jQuery(selected_tab).fadeIn();
		return false;
	});


	//NAV TO SELECT
    jQuery("<select />").addClass("tabs_nav").appendTo("#tabs_nav_wrap");
    jQuery("<option />", {
       "selected": "selected",
       "value"   : "",
       "text"    : "-- Select categories --"
    }).appendTo("#tabs_nav_wrap select");
    jQuery("#tabs_nav_wrap a").each(function() {
     var el = jQuery(this);
     jQuery("<option />", {
         "value"   : el.attr("href"),
         "text"    : el.text()
     }).appendTo("#tabs_nav_wrap select");
    });
    jQuery("#tabs_nav_wrap select").change(function() {
      window.location = jQuery(this).find("option:selected").val();
    });


}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();
});
/* END ------------------------------------------------------- */



